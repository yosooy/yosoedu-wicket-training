#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
YosoEdu - Simplified Twitter in Wicket
\end_layout

\begin_layout Author
Jakub Danek
\end_layout

\begin_layout Standard
.
\end_layout

\begin_layout Section*
Goals
\end_layout

\begin_layout Standard
Implement simplified version of Twitter fullfilling the following goals
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Inspired by lectures of KIV/PIA at The Faculty of Applied Sciences of The
 University of West Bohemia in Pilsen, Czech Republic
\end_layout

\end_inset

:
\end_layout

\begin_layout Subsection*
General
\end_layout

\begin_layout Enumerate
Every action which results in changed data should be followed by a message.
 User wants to see that something has really happened!
\end_layout

\begin_layout Subsection*
Tweets
\end_layout

\begin_layout Enumerate
User can add new tweets.
\end_layout

\begin_layout Enumerate
User can retweet tweets of other users.
\end_layout

\begin_layout Enumerate
User can list his own tweets.
\end_layout

\begin_layout Enumerate
User can list all tweets visible to him (his and of the people he follows).
\end_layout

\begin_layout Enumerate
Demonstrate use of pagination on the list of tweets.
\end_layout

\begin_layout Subsection*
Registration & User Profile
\end_layout

\begin_layout Enumerate
Password is not displayed in plaintext during registration.
\end_layout

\begin_layout Enumerate
Password must be confirmed by repeated input.
\end_layout

\begin_layout Enumerate
User must be able to upload a profile picture on registration.
\begin_inset Newpage clearpage
\end_inset


\end_layout

\begin_layout Enumerate
The picture should be displayed somewhere :o)
\end_layout

\begin_layout Enumerate
User should pass simple Turing test on registration to prove he is truly
 a human.
\end_layout

\begin_deeper
\begin_layout Enumerate
Application should generate two random small numbers and ask user to provide
 their sum.
\end_layout

\end_deeper
\begin_layout Subsection*
User Interaction
\end_layout

\begin_layout Enumerate
User can (un)follow other users.
\end_layout

\begin_layout Enumerate
User can list profiles of his followers and the people he follows.
\end_layout

\begin_layout Section*
Provided Application
\end_layout

\begin_layout Standard
Core part of the application is implemented.
 If something is missing, most likely you don't need it.
 In case you feel like you need additional functionality implemented in
 core, contact your supervisor :o).
\end_layout

\begin_layout Standard
The application is written in Java 7 and uses Apache Wicket 6.5.0.
\end_layout

\begin_layout Subsection*
Data Model
\end_layout

\begin_layout Standard
Main entities are 
\family typewriter
UserDTO
\family default
 and 
\family typewriter
TwitterDTO
\family default
.
 Both are serializable and usable directly in the webapp.
\end_layout

\begin_layout Subsection*
Data Access Objects
\end_layout

\begin_layout Standard
The following example shows how to get an instance of both the application
 DAOs (which should be used to interact with core functionality):
\end_layout

\begin_layout LyX-Code
UserDao userDao = DaoFactoryJDBC.getInstance().getUserDao();
\end_layout

\begin_layout LyX-Code
TwitterDao twitterDao = DaoFactoryJDBC.getInstance().getTwitterDao();
\end_layout

\begin_layout Subsection*
Webapp
\end_layout

\begin_layout Standard
Skeleton for the web application is provided (without markup).
 Base WicketApplication class with WebSession implementation supporting
 authentication is implemented.
 Custom component for date input is provided to demonstrate how Wicket component
 should be written.
\end_layout

\begin_layout Standard
All Page classes are created, waiting for implementation.
 You shouldn't be required to create any more Page classes (except for error
 Pages if needed).
\end_layout

\begin_layout Section*
Running the application
\end_layout

\begin_layout Standard
Application can be started with the following command:
\end_layout

\begin_layout LyX-Code
mvn jetty:run
\end_layout

\begin_layout Standard
and can be aftewards found at:
\end_layout

\begin_layout LyX-Code
http://localhost:8080/
\end_layout

\end_body
\end_document
