/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao;

import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;
import java.io.InputStream;
import java.util.List;

/**
 * User data access object. Provides access to User entity persistence.
 *
 * @author Jakub Danek
 */
public interface UserDao extends GenericDao<UserDTO> {

    /**
     *
     * @param username
     * @param password
     * @return true if username and password form a valid pair
     * @throws GenericDaoException
     */
    public boolean authenticate(String username, String password) throws GenericDaoException;

    /**
     * Search by username
     *
     * @param userName username the object is supposed to have
     * @return User object
     * @throws GenericDaoException
     */
    public UserDTO get(String userName) throws GenericDaoException;

    /**
     * Returns list of users the userId is following
     * @param userId userid
     * @return
     * @throws GenericDaoException
     */
    public List<UserDTO> listFollowing(long userId) throws GenericDaoException;

    /**
     * Returns list of users following the userId
     * @param userId userID
     * @return
     * @throws GenericDaoException
     */
    public List<UserDTO> listFollowers(long userId) throws GenericDaoException;

    /**
     *
     * @param followerId
     * @param followedId
     * @return true if followerId, followedId are a valid pair
     * @throws GenericDaoException
     */
    public boolean follows(long followerId, long followedId) throws GenericDaoException;

    /**
     * Creates new connection between followerId and followedId
     * @param followerId
     * @param followedId
     * @throws GenericDaoException
     */
    public void addFollower(long followerId, long followedId) throws GenericDaoException;

    /**
     * Removes connection between followerId and followedId
     * @param followerId
     * @param followedId
     * @throws GenericDaoException
     */
    public void removeFollower(long followerId, long followedId) throws GenericDaoException;

    /**
     * Counts how many users the userId is following
     * @param userId
     * @return
     * @throws GenericDaoException
     */
    public int countFollowing(long userId) throws GenericDaoException;

    /**
     * Counts how many followers the userId has
     * @param userId
     * @return
     * @throws GenericDaoException
     */
    public int countFollowers(long userId) throws GenericDaoException;

    /**
     * Saves a profile image for the userId
     * @param userId user Id
     * @param stream input stream containing image data
     * @throws GenericDaoException
     */
    public void saveProfileImage(long userId, InputStream stream) throws GenericDaoException;

    /**
     * Load profile image for a user
     * @param userId userId
     * @return input stream containing image data
     * @throws GenericDaoException
     */
    public InputStream loadProfileImage(long userId) throws GenericDaoException;

}
