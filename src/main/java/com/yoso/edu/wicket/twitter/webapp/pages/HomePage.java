package com.yoso.edu.wicket.twitter.webapp.pages;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.model.StringResourceModel;

/**
 * Application homepage. Provides sign-in form and link to registration form.
 * 
 * @author Jakub Danek
 */
public class HomePage extends AbstractPage {
	private static final long serialVersionUID = 1L;

    public HomePage(final PageParameters parameters) {
        super(parameters, new StringResourceModel("page.title.home", null));


    }
}