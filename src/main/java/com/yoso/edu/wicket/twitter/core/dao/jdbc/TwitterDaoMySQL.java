/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao.jdbc;

import static com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoUtilJDBC.*;

import com.yoso.edu.wicket.twitter.core.dao.TwitterDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dto.TwitterDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * JDBC implementation of the TwitterDao interface.
 *
 * @author Jakub Danek
 */
public class TwitterDaoMySQL implements TwitterDao, Serializable {
    
    private DaoFactoryJDBC daoFactory;

    /**
     * Package protected constructor to be used only within JDBC package
     * @param factory factory which will provide database connection
     */
    TwitterDaoMySQL(DaoFactoryJDBC factory) {
        this.daoFactory = factory;
    }

    @Override
    public TwitterDTO get(Long id) throws GenericDaoException {
        //no need in this project, not implemented
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void remove(Long id) throws GenericDaoException {
        //no need in this school project, not implemented
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Long create(TwitterDTO obj) throws GenericDaoException {
        String sql1 = "INSERT INTO tweet (author, date_created, text) VALUES (?, ?, ?);";
        
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        Long id = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);

            st.setLong(1, obj.getAuthorId());
            st.setDate(2, toSqlDate(obj.getCreatedDate()));
            st.setString(3, obj.getTweetText());

            st.executeUpdate();
            res = st.getGeneratedKeys();
            
            if(res.next()) {
                id = res.getLong(1);
            } else {
                throw new GenericDaoException("Tweet record hasnt been created!");
            }

            this.addRetweeter(id, obj.getAuthorId());
        } catch (SQLException ex) {
            throw new GenericDaoException("Tweet record hasnt been created!");
        } finally {
            close(conn, st, res);
        }
        
        return id;
    }

    @Override
    public void update(TwitterDTO obj) throws GenericDaoException {
        //no need in this school project, not implemented
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TwitterDTO> listUsersTweets(Long userId) throws GenericDaoException {
        List<Long> idWrapper = new ArrayList<Long>();
        idWrapper.add(userId);

        return this.listTweetsFromMultipleUsers(idWrapper);
    }

    @Override
    public List<TwitterDTO> listTweetsFromMultipleUsers(List<Long> userIds) throws GenericDaoException {
        String sql = "SELECT DISTINCT t.id, t.author, t.text, t.date_created FROM tweet t, user_tweet j WHERE j.tweet_id = t.id AND (";
        StringBuilder build = new StringBuilder(sql);
        for(int i = 0; i < userIds.size(); i++) {
            build.append("j.owner_id = ?");
            if(i < userIds.size() - 1) {
                build.append(" OR ");
            } else {
                build.append(") ORDER BY t.date_created DESC;");
            }
        }
        sql = build.toString();
        
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        List<TwitterDTO> tweets = new ArrayList<TwitterDTO>();

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            for(int i = 1; i <= userIds.size(); i++) {
                st.setLong(i, userIds.get(i - 1));
            }

            res = st.executeQuery();

            while(res.next()) {
                tweets.add(processSingleResult(res));
            }

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable to get the list of tweets.");
        } finally {
            close(conn, st, res);
        }

        return tweets;
    }

    @Override
    public int countTweets(long userId) throws GenericDaoException {
        String sql = "SELECT count(*) from user_tweet WHERE owner_id = ?;";

        return countById(daoFactory, sql, userId);
    }

    @Override
    public void addRetweeter(long tweetId, long userId) throws GenericDaoException {
        String sql2 = "INSERT INTO user_tweet(tweet_id, owner_id) VALUES (?, ?);";

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql2);
            st.setLong(1, tweetId);
            st.setLong(2, userId);

            st.executeUpdate();
        } catch (SQLException ex) {
            throw new GenericDaoException("Unable to add retweeter.");
        } finally {
            close(conn, st);
        }

    }

    /**
     * Process single resultSet item into TwitterDTO
     * @param set result set with pointer set to the row which should be processed
     * @return the TwitterDTO
     * @throws SQLException when invalid columns queried for values
     */
    private TwitterDTO processSingleResult(ResultSet set) throws GenericDaoException {
        TwitterDTO tweet = new TwitterDTO();

        try {
            tweet.setId(set.getLong("t.id"));
            tweet.setAuthorId(set.getLong("t.author"));
            tweet.setTweetText(set.getString("t.text"));
            tweet.setCreatedDate(toLangDate(set.getDate("t.date_created")));
        } catch (SQLException ex) {
            throw new GenericDaoException("Unable to recover data from the result.");
        }

        tweet.addRetweets(getRetweets(tweet.getId(), tweet.getAuthorId()));

        return tweet;
    }

    /**
     * Retrieve list of user which have retweeted the tweet
     *
     * @param tweetId the tweet
     * @param authorId author id which shall be ommited from the search
     * @return list of retweeters
     */
    private List<Long> getRetweets(long tweetId, long authorId) throws GenericDaoException {
        List<Long> retweets = new ArrayList<Long>();

        String sql = "SELECT owner_id FROM user_tweet WHERE tweet_id = ? AND owner_id != ?;";

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, tweetId);
            st.setLong(2, authorId);

            res = st.executeQuery();

            while(res.next()) {
                retweets.add(res.getLong("owner_id"));
            }

        } catch(SQLException ex) {
            //TODO handle exception
        } finally {
            close(conn, st, res);
        }

        return retweets;
    }
}
