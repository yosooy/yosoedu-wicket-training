/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao.jdbc;

import com.yoso.edu.wicket.twitter.core.dao.DaoFactory;
import com.yoso.edu.wicket.twitter.core.dao.DaoProperties;
import com.yoso.edu.wicket.twitter.core.dao.TwitterDao;
import com.yoso.edu.wicket.twitter.core.dao.UserDao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * JDBC implementation of DaoFactory interface.
 *
 * Creates instances of dao's which use JDBC to connect to a database.
 *
 * @author Jakub Danek
 */
public class DaoFactoryJDBC implements DaoFactory, Serializable {

    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_DRIVER = "driver";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_PASSWORD = "password";

    private String url;
    private String username;
    private String password;

    private DaoFactoryJDBC(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * Returns a new DaoFactoryJDBC instance
     * @return A new DaoFactoryJDBC instance
     */
    public static DaoFactoryJDBC getInstance() {

        DaoProperties properties = new DaoProperties("jdbc");
        String url = properties.getProperty(PROPERTY_URL, true);
        String driver = properties.getProperty(PROPERTY_DRIVER, true);
        String password = properties.getProperty(PROPERTY_PASSWORD, false);
        String username = properties.getProperty(PROPERTY_USERNAME, password != null);
        DaoFactoryJDBC instance;

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(
                "Driver class '" + driver + "' is missing in classpath.", e);
        }

        instance = new DaoFactoryJDBC(url, username, password);

        return instance;
    }

    /**
     * Returns a connection to the database. Package private so that it can be used inside the DAO
     * package only.
     * @return A connection to the database.
     * @throws SQLException If acquiring the connection fails.
     */
    Connection getConnection() throws SQLException {
         return DriverManager.getConnection(url, username, password);
    }

    // DAO implementation getters -----------------------------------------------------------------

    @Override
    public TwitterDao getTwitterDao() {
        return new TwitterDaoMySQL(this);
    }

    @Override
    public UserDao getUserDao() {
        return new UserDaoMySQL(this);
    }
}
