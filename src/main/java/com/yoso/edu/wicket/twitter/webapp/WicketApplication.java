package com.yoso.edu.wicket.twitter.webapp;

import com.yoso.edu.wicket.twitter.webapp.pages.HomePage;
import com.yoso.edu.wicket.twitter.webapp.pages.ProfilePage;
import com.yoso.edu.wicket.twitter.webapp.pages.RegistrationPage;
import com.yoso.edu.wicket.twitter.webapp.pages.SignInPage;
import com.yoso.edu.wicket.twitter.webapp.pages.UserHubPage;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.WebPage;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 *
 */
public class WicketApplication extends AuthenticatedWebApplication {

    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        mountPages();
    }

    /**
     * Creates nice urls for pages.
     */
    private void mountPages() {
        mountPage("registration", RegistrationPage.class);
        mountPage("login", SignInPage.class);
        mountPage("hub", UserHubPage.class);
        mountPage("profile", ProfilePage.class);
        mountPage("main", HomePage.class);
    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return WebAuthSession.class;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return SignInPage.class;
    }

    @Override
    public RuntimeConfigurationType getConfigurationType() {
        return RuntimeConfigurationType.DEVELOPMENT;
    }


}
