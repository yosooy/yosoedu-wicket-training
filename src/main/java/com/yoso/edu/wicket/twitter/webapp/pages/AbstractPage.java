/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.webapp.pages;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Base class for all pages. Provides common layout.
 *
 * @author Jakub Danek
 */
public class AbstractPage extends WebPage {

    /**
     * Constructor with title model argument. Title can be changed via setTitle method.
     *
     * @param params page parameters
     * @param titleStringModel title model
     */
    protected AbstractPage(PageParameters params, IModel<String> titleStringModel) {
        super(params);

        init();
    }

    /**
     * Constructor without title model. The title should be set via setTitle method
     * then.
     * @param params page parameters
     */
    protected AbstractPage(PageParameters params) {
        super(params);

        init();
    }

    private void init() {
        /*
         * TODO here should be global components displayed at all pages - header, menu, global feedback etc.
         */
    }

    protected final void setTitle(String title) {

    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(CssHeaderItem.forUrl("styles/stylesheet.css"));
        /*
         * TODO Additional css and js belong here.
         */
    }


}
