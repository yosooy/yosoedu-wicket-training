/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.webapp;

import com.yoso.edu.wicket.twitter.core.dao.UserDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoFactoryJDBC;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.Request;

/**
 *
 * @author Jakub Danek
 */
public class WebAuthSession extends AuthenticatedWebSession {

    private IModel<UserDTO> currentUser;

    public WebAuthSession(Request request) {
        super(request);
        currentUser = new Model<UserDTO>();
    }

    @Override
    public boolean authenticate(String username, String password) {
        try {
            UserDao userDao = DaoFactoryJDBC.getInstance().getUserDao();

            boolean auth = userDao.authenticate(username, password);

            if(auth) {
                currentUser.setObject(userDao.get(username));
            } else {
                currentUser.setObject(null);
            }

            return auth;
        } catch (GenericDaoException ex) {
            return false;
        }
    }

    @Override
    public void signOut() {
        super.signOut();
        this.currentUser.setObject(null);
    }

    @Override
    public Roles getRoles() {
        Roles roles = new Roles();
        
        if(isSignedIn()) {
            roles.add(Roles.USER);
        }
        
        return roles;
    }

    /**
     *
     * @return model with the current user's data object
     */
    public IModel<UserDTO> getCurrentUserModel() {
        return currentUser;
    }

    /**
     *
     * @return current user's id or null if guest
     */
    public Long getCurrentUserId() {
        if(currentUser.getObject() == null) {
            return null;
        } else {
            return currentUser.getObject().getId();
        }
    }

    /**
     *
     * @return current session object
     */
    public static WebAuthSession get() {
        return (WebAuthSession) AuthenticatedWebSession.get();
    }

}
