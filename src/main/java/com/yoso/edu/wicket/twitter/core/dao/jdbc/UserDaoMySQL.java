/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao.jdbc;

import static com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoUtilJDBC.*;

import com.yoso.edu.wicket.twitter.core.dao.UserDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dto.Sex;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;

import java.io.InputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * JDBC implementation of the UserDao interface.
 *
 * @author Jakub Danek
 */
public class UserDaoMySQL implements UserDao, Serializable {
    
    private DaoFactoryJDBC daoFactory;

    /**
     * Package protected, to be used only within JDBC package
     * @param factory factory which will provide db connection
     */
    UserDaoMySQL(DaoFactoryJDBC factory) {
        this.daoFactory = factory;
    }

    @Override
    public UserDTO get(Long id) throws GenericDaoException {
        String sql = "SELECT id, username, first_name, last_name, date_of_birth, sex, additional_info FROM app_user u WHERE u.id = ?";

        List<UserDTO> result = searchById(sql, id);

        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    @Override
    public void remove(Long id) throws GenericDaoException {
        String sql = "DELETE FROM app_user u WHERE u.id = ?";

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, id);
            st.executeUpdate();

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable remove user");
        } finally {
            close(conn, st);
        }
    }

    @Override
    public Long create(UserDTO obj) throws GenericDaoException {
        String sql = "INSERT INTO app_user(username, password, first_name, last_name, date_of_birth, additional_info, sex) "
                + "VALUES (?,sha1(?),?,?,?,?,?);";

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet key = null;
        Long newId;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

            st.setString(1, obj.getUserName());
            st.setString(2, obj.getPassword());
            st.setString(3, obj.getFirstName());
            st.setString(4, obj.getLastName());
            st.setDate(5, toSqlDate(obj.getDateOfBirth()));
            st.setString(6, obj.getAdditionalInfo());

            if(obj.getSex() != null) {
                st.setInt(7, obj.getSex().ordinal());
            } else {
                st.setObject(7, null);
            }

            st.executeUpdate();
            key = st.getGeneratedKeys();
            key.next();
            newId = key.getLong(1);

        } catch (SQLException ex) {
            throw new GenericDaoException("Unable to create new user");
        } finally {
            close(conn, st, key);
        }

        return newId;
    }

    @Override
    public void update(UserDTO obj) throws GenericDaoException {
        //not required in this school project, not implemented
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean authenticate(String username, String password) throws GenericDaoException {
        String sql = "SELECT id FROM app_user u WHERE u.username = ? AND u.password = sha1(?);";

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;

        boolean valid = false;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);

            res = st.executeQuery();

            valid = res.next();

        } catch (SQLException ex) {
            throw new GenericDaoException("Unable to authenticate user");
        } finally {
            close(conn, st, res);
        }

        return valid;
    }

    @Override
    public UserDTO get(String userName) throws GenericDaoException {
        String sql = "SELECT id, username, first_name, last_name, date_of_birth, sex, additional_info FROM app_user u WHERE u.username = ?";

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        UserDTO user = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setString(1, userName);

            res = st.executeQuery();

            if(res.next()) {
                user = processSingleResult(res);
            }

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable to get the user");
        } finally {
            close(conn, st, res);
        }

        return user;
    }

    private UserDTO processSingleResult(ResultSet set) throws GenericDaoException {
        UserDTO user = new UserDTO();

        try {
            user.setId(set.getLong("id"));
            user.setUserName(set.getString("username"));
            user.setFirstName(set.getString("first_name"));
            user.setLastName(set.getString("last_name"));
            user.setDateOfBirth(toLangDate(set.getDate("date_of_birth")));
            Integer ord = (Integer)set.getObject("sex");
            if(ord != null) {
                user.setSex(Sex.values()[ord]);
            } else {
                user.setSex(null);
            }
            user.setAdditionalInfo(set.getString("additional_info"));
        } catch (SQLException ex) {
            throw new GenericDaoException("Unable to process result");
        }

        return user;
    }

    @Override
    public List<UserDTO> listFollowing(long userId) throws GenericDaoException {
        String sql = "SELECT id, username, first_name, last_name, date_of_birth, sex, additional_info FROM app_user u, followers f WHERE u.id = f.following AND f.follower = ?;";

        return searchById(sql, userId);
    }

    @Override
    public List<UserDTO> listFollowers(long userId) throws GenericDaoException {
        String sql = "SELECT id, username, first_name, last_name, date_of_birth, sex, additional_info FROM app_user u, followers f WHERE u.id = f.follower AND f.following = ?;";

        return searchById(sql, userId);
    }

    @Override
    public int countFollowing(long userId) throws GenericDaoException {
        String sql = "SELECT count(*) FROM followers f WHERE f.follower = ?";

        return countById(daoFactory, sql, userId);
    }

    @Override
    public int countFollowers(long userId) throws GenericDaoException {
        String sql = "SELECT count(*) FROM followers f WHERE f.following = ?";

        return countById(daoFactory, sql, userId);
    }

    @Override
    public boolean follows(long followerId, long followedId) throws GenericDaoException {
        String sql = "SELECT count(*) FROM followers f WHERE f.follower = ? AND f.following = ?;";

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        int count = 0;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, followerId);
            st.setLong(2, followedId);

            res = st.executeQuery();

            if(res.next()) {
                count = res.getInt(1);
            }

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable to confirm follower status");
        } finally {
            close(conn, st, res);
        }

        return count == 1;
    }

    @Override
    public void addFollower(long followerId, long followedId) throws GenericDaoException {
        String sql = "INSERT INTO followers (follower, following) VALUES(?, ?);";

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, followerId);
            st.setLong(2, followedId);

            st.executeUpdate();

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable to add follower");
        } finally {
            close(conn, st);
        }

    }

    @Override
    public void removeFollower(long followerId, long followedId) throws GenericDaoException {
        String sql = "DELETE FROM followers WHERE follower = ? AND following = ?";

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, followerId);
            st.setLong(2, followedId);

            st.executeUpdate();

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable to remove follower");
        } finally {
            close(conn, st);
        }
    }

    /**
     * Helper method for search for list of UserDTOs by single long value.
     * @param sql sql query
     * @param userId long value to be searched by
     * @return list of found UserDTOs
     */
    private List<UserDTO> searchById(String sql, long userId) throws GenericDaoException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        List<UserDTO> users = new ArrayList<UserDTO>();

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, userId);

            res = st.executeQuery();

            while(res.next()) {
                users.add(processSingleResult(res));
            }

        } catch(SQLException ex) {
            throw new GenericDaoException("Error during database search.");
        } finally {
            close(conn, st, res);
        }

        return users;
    }

    @Override
    public void saveProfileImage(long userId, InputStream stream) throws GenericDaoException {
        String sql = "INSERT INTO user_image (user_id, image) VALUES (?, ?);";

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, userId);
            st.setBlob(2, stream);

            st.executeUpdate();

        } catch(SQLException ex) {
            throw new GenericDaoException("Unable to save image", ex);
        } finally {
            close(conn, st);
        }
    }

    @Override
    public InputStream loadProfileImage(long userId) throws GenericDaoException {
        String sql = "SELECT image FROM user_image WHERE user_id = ?;";
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        InputStream imageData = null;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, userId);

            res = st.executeQuery();

            while(res.next()) {
                Blob b = res.getBlob("image");
                if(b != null) {
                    imageData = b.getBinaryStream();
                }
            }

        } catch(SQLException ex) {
            throw new GenericDaoException("Error during image load.");
        } finally {
            close(conn, st, res);
        }

        return imageData;
    }

}