package com.yoso.edu.wicket.twitter.webapp.components.form.input;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.Model;

/**
 * Date chooser component that represents date with three dropdown for
 * day, month and year.
 *
 * Copied from http://blog.aparnachaudhary.net/2009/07/29/date-dropdownchoice-apache-wicket/
 *
 * Modified to use generics.
 *
 * @author Jakub Danek
 */
public class DateChooser extends FormComponentPanel<Date> {

    /** The day ddc. */
    private DropDownChoice<Integer> dayDDC;
    /** The month ddc. */
    private DropDownChoice<Integer> monthDDC;
    /** The year ddc. */
    private DropDownChoice<Integer> yearDDC;
    public static final int DEFAULT_MIN_YEAR = 1900;
    private Date maxDate;
    private Date minDate;

    /**
     * Datechooser without date boundaries
     *
     * @param id   id of the component
     */
    public DateChooser(final String id) {
        this(id, null, null);
    }

    /**
     * Datechooser with boundaries
     *
     * @param id id of the component
     * @param minDate lowest available date
     * @param maxDate highest available date
     */
    public DateChooser(final String id, Date minDate, Date maxDate) {
        super(id);

        this.maxDate = maxDate != null ? maxDate : new Date();
        if (minDate == null) {
            Calendar c = Calendar.getInstance();
            c.set(DEFAULT_MIN_YEAR, Calendar.JANUARY, 1);
            this.minDate = c.getTime();
        } else {
            this.minDate = minDate;
        }

        addAndInitComponents();
    }

    private void addAndInitComponents() {
        yearDDC = new DropDownChoice<Integer>("year", new Model<Integer>(), getYears());
        yearDDC.setNullValid(true);

        add(yearDDC);
        yearDDC.add(new AjaxFormComponentUpdatingBehavior("onchange") {

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                //change dayDDC choices when year changes
                dayDDC.setChoices(getDays());
                target.add(dayDDC);
            }
        });

        monthDDC = new DropDownChoice<Integer>("month", new Model<Integer>(), getMonths(), new IChoiceRenderer<Integer>() {

            @Override
            public Object getDisplayValue(Integer object) {
                SimpleDateFormat format = new SimpleDateFormat("MMM");

                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, object);

                return format.format(cal.getTime());
            }

            @Override
            public String getIdValue(Integer integer, int index) {
                return String.valueOf(index);
            }
        });

        monthDDC.setNullValid(true);

        monthDDC.add(new AjaxFormComponentUpdatingBehavior("onchange") {

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                // change the days dropdown when month changes
                dayDDC.setChoices(getDays());
                target.add(dayDDC);
            }
        });
        add(monthDDC);

        dayDDC = new DropDownChoice<Integer>("day", new Model<Integer>(), getDays());
        dayDDC.setNullValid(true);
        dayDDC.setOutputMarkupId(true);
        add(dayDDC);
    }

    public void setMaxDate(Date date) {
        this.maxDate = date;
    }

    public void setMinDate(Date date) {
        this.minDate = date;
    }

    /**
     * Gets the months.
     *
     * @return the months
     */
    private List<Integer> getMonths() {
        List<Integer> months = new ArrayList<Integer>(12);
        Calendar cal = Calendar.getInstance(this.getLocale());
        cal.setTime(maxDate);

        int maxMonth = 11;

        for (int i = 0; i <= maxMonth; i++) {
            months.add(i);
        }

        return months;
    }

    /**
     * Gets the days.
     *
     * @return the days
     */
    protected List<Integer> getDays() {
        List<Integer> days = new ArrayList<Integer>(31);
        int totalDays = 31;
        Calendar cal = Calendar.getInstance();

        if (yearDDC.getModelObject() != null && monthDDC.getModelObject() != null) {
            cal.set(yearDDC.getModelObject(), monthDDC.getModelObject(), 1);
            totalDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        cal.setTime(maxDate);
        int maxDate = totalDays;

        for (int i = 1; i <= maxDate; i++) {
            days.add(i);
        }
        return days;
    }

    /**
     * Gets the years.
     *
     * @return the years
     */
    protected List<Integer> getYears() {
        List<Integer> years = new ArrayList<Integer>(10);

        Calendar cal = Calendar.getInstance();
        cal.setTime(maxDate);
        int maxYear = cal.get(Calendar.YEAR);

        cal.setTime(minDate);
        int minYear = cal.get(Calendar.YEAR);

        for (int i = minYear; i <= maxYear; i++) {
            years.add(i);
        }

        return years;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.apache.wicket.markup.html.form.FormComponent#convertInput()
     */
    @Override
    protected void convertInput() {
        Calendar cal = Calendar.getInstance();
        if(yearDDC.getConvertedInput() != null) {
            Integer month = monthDDC.getConvertedInput() != null ? monthDDC.getConvertedInput() : 0;
            Integer day = dayDDC.getConvertedInput() != null ? dayDDC.getConvertedInput() : 0;
            cal.set(yearDDC.getConvertedInput(), month, day, 0, 0, 0);
            Date selectedDate = cal.getTime();
            setConvertedInput(selectedDate);
        } else {
            setConvertedInput(null);
        }
    }

    @Override
    protected void onModelChanged() {
        super.onModelChanged();

        Date date = getModelObject();
        GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
        if (date != null) {
            cal.setTime(date);
            dayDDC.setModelObject(cal.get(GregorianCalendar.DAY_OF_MONTH));
            monthDDC.setModelObject(cal.get(GregorianCalendar.MONTH));
            yearDDC.setModelObject(cal.get(GregorianCalendar.YEAR));
        } else {
            dayDDC.setModelObject(null);
            monthDDC.setModelObject(null);
            yearDDC.setModelObject(null);
        }
        
    }
}
